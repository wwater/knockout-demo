﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnockoutDemo.Models
{
    public class HourModel
    {
        public string Employee { get; set; }

        public int Hours { get; set; }

        public int Costs { get; set; }

        public int EmployeeId { get; set; }
    }
}