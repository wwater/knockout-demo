﻿using KnockoutDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KnockoutDemo.Controllers
{
    public class HoursController : ApiController
    {
        // GET api/<controller>
        public HourModel[] Get()
        {
            return new HourModel[] 
            { 
                new HourModel(){EmployeeId = 1, Employee = "pietje", Hours = 5, Costs = 3},
                new HourModel(){EmployeeId = 2, Employee = "henkie", Hours = 8, Costs = 6}
            };
        }

        // GET api/<controller>/5
        public HourModel Get(int id)
        {
            return new HourModel() { EmployeeId = 2, Employee = "henkie", Hours = 55, Costs = 66 };
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}