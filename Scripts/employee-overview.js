﻿/// <reference path="knockout-3.0.0.js" />
$(function () {
    ko.applyBindings(new EmployeesViewModel());
});

function EmployeesViewModel() {
    var self = this;

    self.employees = ko.observableArray([]);

    var mapping = {
        create: function (options) {
            return new EmployeeViewModel(options.data);
        }
    }

    $.getJSON('/api/hours', function (data) {
        ko.mapping.fromJS(data, mapping, self.employees);
    });

    self.TotalHours = ko.computed(function () {
        var result = 0;
        ko.utils.arrayForEach(self.employees(), function (item) {
            result += parseFloat(item.Hours());
        });
        return result;
    })

    self.TotalCosts = ko.computed(function () {
        var result = 0;
        ko.utils.arrayForEach(self.employees(), function (item) {
            result += parseFloat(item.TotalCosts());
        });
        return result;
    })
}

function EmployeeViewModel(data)
{
    var self = this;
    ko.mapping.fromJS(data, {}, self);

    self.TotalCosts = ko.computed(function()
    {
        return parseFloat(self.Costs()) * parseFloat(self.Hours());
    })
}

ko.bindingHandlers.slideVisible = {
    init: function (element, valueAccessor) {
        // Initially set the element to be instantly visible/hidden depending on the value
        var value = valueAccessor();
        $(element).toggle(ko.unwrap(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
    },
    update: function (element, valueAccessor, allBindings)
    {
        // First get the latest data that we're bound to
        var value = valueAccessor();

        // Next, whether or not the supplied model property is observable, get its current value
        var valueUnwrapped = ko.unwrap(value);

        // Grab some more data from another binding property
        var duration = allBindings.get('slideDuration') || 400; // 400ms is default duration unless otherwise specified

        // Now manipulate the DOM element
        if (valueUnwrapped == true)
            $(element).slideDown(duration); // Make the element visible
        else
            $(element).slideUp(duration);   // Make the element invisible
    }
};

ko.bindingHandlers.textHighlightChange = {
    initValue: null,
    init: function (element, valueAccessor) {
        initValue = ko.unwrap(valueAccessor());
        $(element).text(initValue);
    },
    update: function (element, valueAccessor) {
        var valueUnwrapped = ko.unwrap(valueAccessor());
        if (valueUnwrapped !== this.initValue) {
            $(element).fadeOut("slow", function () { $(element).text(valueUnwrapped) }).fadeIn("slow");
        }
    }
};